/*
 * File: bitPatterns.cpp
 * ---------------------------
 * Name: Rajiv McCoy
 * This file implements a recursive function to generate all possible
 * binary numbers of a given length.
 */


#include <iostream>
#include <string>
#include "simpio.h"
#include "console.h"
#include "set.h"
using namespace std;

/* Prototypes */

Set<string> generateBinaryCode(int bits);


/* Main Program */

int main() {
    int n = getInteger("Please enter the number of bits: ");
    Set<string> results = generateBinaryCode(n);
    for (string str : results) {
        cout << str << endl;
    }
    return 0;
}


/* Recursive function to generate all possible n bit numbers */

Set<string> generateBinaryCode(int bits) {
    Set<string> results;
    if (bits == 0) {
        results += "";
    } else {
        for (string s : generateBinaryCode(bits - 1)) {
            results += ('0' + s);
            results += ('1' + s);
        }
    }
    return results;
}
