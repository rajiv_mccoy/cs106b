/*
 * File: KarelGoesHome.cpp
 * -----------------------
 * Counts the possible shortest routes home for Karel subject to
 * the restriction that he only move south or west.
 */

#include <iostream>
#include <iomanip>
#include "grid.h"
#include "simpio.h"
#include "console.h"
using namespace std;

/* Function prototypes */

int countPaths(int street, int avenue, int & nPaths);

/* Main program */

int main() {
   int street = getInteger("Please enter street: ");
   int avenue = getInteger("Please enter avenue: ");
   int nPaths = 0;
   nPaths = countPaths(street, avenue, nPaths);
   cout << "There are " << nPaths << " possible shortest routes home from your current position.";
   return 0;
}

/*
 * Function: countPaths
 * Usage: int nPaths = countPaths(street, avenue);
 * -----------------------------------------------
 * Counts the paths from a particular street/avenue position in
 * Karel's world to the origin, subject to the restriction that
 * Karel can move only west and south.
 */

int countPaths(int street, int avenue, int & nPaths) {

    Grid<int> grid(street + 1, avenue + 1); // necessary because grid is 0-based

    if (street == 1 && avenue == 1) {
        nPaths++;
    } else {
        if (street > 0 && avenue > 0) {
            countPaths(street - 1, avenue, nPaths);
            countPaths(street, avenue - 1, nPaths);
        }
    }
    return nPaths;
}
